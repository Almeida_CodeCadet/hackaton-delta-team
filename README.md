**Team Delta Bowie Project - Backend**

Our e-commerce project Developed in the context of <Academia de Código_> hackathon challenge (24h non-stop programming).

**Developed by:** André Castro, Juliana Liberali, Pedro Gaspar, Hugo Leal, Luís Almeida

The Backend is a REST API that can be consumed by other applications.
It was implemented using a MVC architecture with distint layers depending on its functions: DAO/Services/Controllers

**Technologies**

- Java
- Maven project for easier dependencies management
- Spring MVC Framework
- Hibernate for persistence
- MySQL for database
- REST
- Tomcat


**Current State**

- All CRUD operations are implemented
- Controllers methods support all defined requests from the frontend


**Next Steps**

- GoLive - Heroku deployment
