package org.academiadecodigo.hackathon.persistence.model.product;

public enum ProductType {

    VIRTUAL_EXPERIENCES,
    REAL_EXPERIENCES,
    MISCELLANEOUS
}
